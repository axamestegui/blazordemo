﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BlazoDemo.Shared;

namespace BlazoDemo.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        [HttpGet]
        [Route("all")]
        public IEnumerable<PersonModel> GetPeople()
        {
            var output = new List<PersonModel>
            {
                new PersonModel { FirstName = "Tim", LastName = "Corey", AccountBalance = 108.46M},
                new PersonModel { FirstName = "Mary", LastName = "Dream", AccountBalance = 128.76M},
                new PersonModel { FirstName = "John", LastName = "Smith", AccountBalance = 15.26M}
            };

            return output;
        }

        [HttpPost]
        public void Post(PersonModel person)
        {
            //Do your stuff on the server
        }
    }
}